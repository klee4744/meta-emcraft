# Copyright (C) 2021 Emcraft Systems

STM32MP_SOURCE_SELECTION = "github"
SRC_URI_class-devupstream = "git://gitlab.com/emcraft/stm32mp15x/u-boot-stm32mp.git;protocol=https;branch=v2020.01-stm32mp-som"

SRCREV_class-devupstream = "${AUTOREV}"
